<?php

namespace AbraD450;

/**
 * Files Tools
 */
class MimeTypes
{

    /**
     * MIME ext file
     * @var string
     */
    const MIME_FILE = 'mime.json';

    /**
     * Cache of ext -> mime from file mime.ext
     * @var array
     */
    static private array $mimeExt = [];

    /**
     * Returns MIME type for given extension
     *
     * @param string $extension File Extension (i.e. pdf)
     * @param string $default What to return when extension is not found (DEFAULT: application/octet-stream)
     * @param string $url URL where to find mime.types file from apache (DEFAULT: https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types)
     * @return string
     */
    static public function get(string $extension, string $default = 'application/octet-stream', string $url = 'https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types'): string
    {
        if(empty(self::$mimeExt)) {
            self::cacheMimeExt($url);
        }
        return self::$mimeExt[trim(strtolower($extension))] ?? $default;
    }
    
    /**
     * Cache MIME hashmap to memory
     * 
     * @return void
     */
    static private function cacheMimeExt(string $url): void
    {
        // if there is no mime.json file yet, download it
        if(!is_file(__DIR__.'/'.self::MIME_FILE)) {
            self::downloadMimeExt($url);
        }
        self::$mimeExt = json_decode(file_get_contents(__DIR__ . '/' . self::MIME_FILE), TRUE);
    }
    
    /**
     * Download MIME file from URL
     * 
     * @param string $url
     */
    static private function downloadMimeExt(string $url): void
    {
        self::$mimeExt = [];
        $file = file($url, 0, stream_context_create([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
            'http' => [
                'method' => "GET",
                'timeout' => 60,
            ]
        ]));
        if(empty($file)) {
            return;
        }
        foreach($file as $line) {
            $lineTrim = trim($line);
            if(strlen($lineTrim) <= 0 || substr($lineTrim, 0, 1) === '#') {
                continue;
            }
            $lineData = preg_split('/\s+/', $lineTrim, -1, PREG_SPLIT_NO_EMPTY);
            if(count($lineData) <= 1) {
                continue;
            }
            $mime = array_shift($lineData);
            foreach($lineData as $ext) {
                self::$mimeExt[$ext] = $mime;
            }
        }
        
        ksort(self::$mimeExt);
        
        file_put_contents(__DIR__.'/'.self::MIME_FILE, json_encode(self::$mimeExt));
    }
}
