# File MIME type detection helper

This helper caches `mime.types` file from Apache to JSON file locally to be able to resolve any MIME type.

## Usage

	$mime = \AbraD450\MimeTypes::get('png');
	
	echo $mime; // prints 'image/png'